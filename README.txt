
-- SUMMARY --

The Driesday module provides a block that you can display any way you like using
the standard Drupal block system.

On November 19, the block will display a special message. On all other days, the
block will tell you that Driesday is coming soon.

-- INSTALLATION --

Install as usual, see http://drupal.org/node/70151 for further information.

-- CUSTOMIZATION --

Block output is controlled by a theme function theme_driesday_get_dries().
Override this function as usual to control the block's output.

-- CONTACT --

Current maintainer: Karl Scheirer (kscheirer) - https://drupal.org/user/128191
